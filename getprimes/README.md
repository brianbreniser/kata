# primenumbers

get all primes up to a number

Example:
    primes(10)
    2,3,5,7

    primes(20)
    2,3,5,7,11,13,17,19

