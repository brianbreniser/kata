package kata;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
    }

    public static boolean isPrime(int n) {
        if (n < 2) return false;

        for (int i = 2; i < n; ++i) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    public static boolean isPrimeSlow(int n) {
        return isPrime(n);
    }

    public static boolean isPrimeFaster(int n) {
        if (n < 2) return false;

        for (int i = 2; i < (n/2 + 1); ++i) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    public List<Integer> getPrimes(int n) {
        if (n < 2) {
            return Arrays.asList();
        }

        List<Integer> result = new ArrayList<Integer>();
        result.add(2);

        for (int i = 3; i <= n; i+=2) {
            if (isPrime(i)) {
                result.add(i);
            }
        }

        return result;
    }
}
