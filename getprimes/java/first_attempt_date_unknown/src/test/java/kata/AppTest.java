package kata;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;

// short prime list
// 2,3,5,7,11,13,17,19

/**
 * Unit test for simple App.
 */
public class AppTest {

    private App app = new App();

    /**
     * Rigorous Test :-)
     */

    @Test
    public void primes0() {
        assertEquals(Arrays.asList(), app.getPrimes(0));
    }

    @Test
    public void primes1() {
        assertEquals(Arrays.asList(), app.getPrimes(1));
    }

    @Test
    public void primes2() {
        assertEquals(Arrays.asList(2), app.getPrimes(2));
    }

    @Test
    public void primes3() {
        assertEquals(Arrays.asList(2,3), app.getPrimes(3));
    }

    @Test
    public void primes4() {
        assertEquals(Arrays.asList(2,3), app.getPrimes(4));
    }

    @Test
    public void primes100() {
        assertEquals(Arrays.asList(2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97), app.getPrimes(100));
    }

    @Test
    public void isprime0() {
        assertFalse(app.isPrime(0));
    }

    @Test
    public void isprime1() {
        assertFalse(app.isPrime(1));
    }

    @Test
    public void isprime2() {
        assertTrue(app.isPrime(2));
    }

    @Test
    public void isprime3() {
        assertTrue(app.isPrime(3));
    }

    @Test
    public void isprime4() {
        assertFalse(app.isPrime(4));
    }

    @Test
    public void isprime5() {
        assertTrue(app.isPrime(5));
    }

    @Test
    public void isprime6() {
        assertFalse(app.isPrime(6));
    }

    @Test
    public void isprime7() {
        assertTrue(app.isPrime(7));
    }

    @Test
    public void isPrimeComparison() {
        for (int i = 0; i < 100; ++i)
            assertEquals(app.isPrimeSlow(i), app.isPrimeFaster(i));
    }
}
