# kata

I've decided to begin daily kata's.

Organized by Kata first, then by language implementation.

Some Kata's might have a base to copy from, or possibly a parent project with multiple implementations underneath.

I don't code as often as I'd like. This will help keep me sharp.

