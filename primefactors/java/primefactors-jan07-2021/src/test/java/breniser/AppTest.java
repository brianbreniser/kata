package breniser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    App app = new App();

    @Test
    public void test0() {
        assertEquals(List.of(), app.factorsOf(0));
    }

    @Test
    public void test1() {
        assertEquals(List.of(), app.factorsOf(1));
    }

    @Test
    public void test2() {
        assertEquals(List.of(2), app.factorsOf(2));
    }

    @Test
    public void test3() {
        assertEquals(List.of(3), app.factorsOf(3));
    }

    @Test
    public void test4() {
        assertEquals(List.of(2,2), app.factorsOf(4));
    }

    @Test
    public void test5() {
        assertEquals(List.of(5), app.factorsOf(5));
    }

    @Test
    public void test6() {
        assertEquals(List.of(2,3), app.factorsOf(6));
    }

    @Test
    public void testbignum() {
        assertEquals(List.of(2,3,5,7,13,13,19), app.factorsOf(2*3*5*7*13*13*19));
    }
}
