package breniser;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public List<Integer> factorsOf(int n) {
        if (n <= 1) return List.of();

        List<Integer> factors = new ArrayList<>();

        for (int i = 2; i <= n; i++)
            for (; n % i == 0; n /= i)
                factors.add(i);

        return factors;
    }
}
