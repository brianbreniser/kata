package primefactorspackage;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public List<Integer> primes(int n) {
        ArrayList<Integer> primes = new ArrayList<>();

        if (n <= 1) return primes;

        for (int i = 2; i <= n; i++)
            for (; n % i == 0; n /= i)
                primes.add(i);

        return primes;
    }
}
