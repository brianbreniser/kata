package primefactorspackage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    App app = new App();

    @Test
    public void test0() {
        assertEquals(List.of(), app.primes(0));
    }

    @Test
    public void test1() {
        assertEquals(List.of(), app.primes(1));
    }

    @Test
    public void test2() {
        assertEquals(List.of(2), app.primes(2));
    }

    @Test
    public void test3() {
        assertEquals(List.of(3), app.primes(3));
    }

    @Test
    public void test4() {
        assertEquals(List.of(2,2), app.primes(4));
    }

    @Test
    public void test5() {
        assertEquals(List.of(5), app.primes(5));
    }

    @Test
    public void testbignum() {
        assertEquals(List.of(2,3,5,7,7,7,13), app.primes(2*3*5*7*7*7*13));
    }
}
