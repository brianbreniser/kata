fun main(args: Array<String>) {
    println("Hello World!")
}

fun getFactors(x: Int): List<Int> {
    if (x <= 1) return ArrayList<Int>()

    var n = x

    val factors = ArrayList<Int>()

    for (i in 2..n) {
        while (n % i == 0) {
            factors.add(i)
            n /= i
        }
    }

    return factors
}