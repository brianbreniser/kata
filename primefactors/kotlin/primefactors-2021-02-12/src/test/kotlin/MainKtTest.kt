import org.junit.Test
import kotlin.test.assertEquals

internal class MainKtTest {
    @Test
    fun test0() {
        assertEquals(getPrimes(0), listOf())
    }

    @Test
    fun test1() {
        assertEquals(getPrimes(1), listOf())
    }

    @Test
    fun test2() {
        assertEquals(getPrimes(2), listOf(2))
    }

    @Test
    fun test3() {
        assertEquals(getPrimes(3), listOf(3))
    }

    @Test
    fun test4() {
        assertEquals(getPrimes(4), listOf(2,2))
    }

    @Test
    fun test5() {
        assertEquals(getPrimes(5), listOf(5))
    }

    @Test
    fun testBigNum() {
        assertEquals(getPrimes(2*2*5*7*13), listOf(2,2,5,7,13))
    }
}