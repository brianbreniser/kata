fun main(args: Array<String>) {
    println("Hello World!")
}

fun getPrimes(a: Int): List<Int> {
    if (a <= 1) {
        return listOf()
    }

    val primes = mutableListOf<Int>()
    var x = a

    for (i in 2..x) {
        while (x % i == 0) {
            primes.add(i)
            x /= i
        }
    }

    return primes
}
