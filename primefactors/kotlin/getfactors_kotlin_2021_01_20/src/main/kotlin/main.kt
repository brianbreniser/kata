fun main(args: Array<String>) {
    println("Hello World!")
}

fun getFactors(n: Int): List<Int> {
    if (n <= 1) return listOf()

    var x = n
    val primes = ArrayList<Int>()

    for (i in 2..x) {
        while (x % i == 0) {
            primes.add(i)
            x /= i
        }
    }

    return primes
}
