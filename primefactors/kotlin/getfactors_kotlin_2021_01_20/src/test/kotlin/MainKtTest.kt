import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MainKtTest {
    @Test
    fun test_0() {
        assertEquals(listOf<Int>(), getFactors(0))
    }

    @Test
    fun test_1() {
        assertEquals(listOf<Int>(), getFactors(1))
    }

    @Test
    fun test_2() {
        assertEquals(listOf(2), getFactors(2))
    }

    @Test
    fun test_3() {
        assertEquals(listOf(3), getFactors(3))
    }

    @Test
    fun test_4() {
        assertEquals(listOf(2,2), getFactors(4))
    }

    @Test
    fun test_bignum() {
        assertEquals(listOf(2,2,3,5,7,11,11,23), getFactors(2*2*3*5*7*11*11*23))
    }
}