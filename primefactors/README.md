# Prime Factors

How many primes can factor into a number?

example primes:
2, 3, 5, 7, 11, 13, 17

Example:
    14
    divisable by 2 to make 7
    7 is prime
    primes: 2, 7

Example:
    25
    Not divisible by 2, 3, but by 5, 5 times
    5 is primt
    primes: 5, 5

Example
    30
    Divices by 2 to 15
    divides by 5 for 3
    primes: 2, 3, 5


