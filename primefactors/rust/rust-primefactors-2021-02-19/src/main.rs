fn main() {
    println!("Hello, world!");
}

fn factors(_n: isize) -> Vec<isize> {
    if _n <= 1 {
        return vec!()
    }

    let mut n = _n.clone();
    let mut r: Vec<isize> = vec!();

    for i in 2..=n {
        while n % i == 0 {
            r.push(i);
            n /= i;
        }
    }

    return r
}

#[cfg(test)]
mod tests {
   use super::*;

    #[test]
    fn test_0() {
        assert_eq!(factors(0), vec!());
    }

    #[test]
    fn test_1() {
        assert_eq!(factors(1), vec!());
    }

    #[test]
    fn test_2() {
        assert_eq!(factors(2), vec!(2));
    }

    #[test]
    fn test_3() {
        assert_eq!(factors(3), vec!(3));
    }

    #[test]
    fn test_4() {
        assert_eq!(factors(4), vec!(2,2));
    }

    #[test]
    fn test_5() {
        assert_eq!(factors(5), vec!(5));
    }

    #[test]
    fn test_6() {
        assert_eq!(factors(6), vec!(2,3));
    }

    #[test]
    fn test_bignum() {
        assert_eq!(factors(2*3*5*7*11*23), vec!(2,3,5,7,11,23));
    }
}
