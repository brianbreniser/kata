fn main() {
    println!("Hello, world!");
}

fn get_prime_factors(n: isize) -> Vec<isize> {
    if n <= 1 {
        return vec!();
    }

    let mut prime_factors: Vec<isize> = vec!();
    let mut n = n;

    for i in 2..=n {
        while n % i == 0 {
            prime_factors.push(i);
            n /= i;
        }
    }



    return prime_factors;

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_0() {
        assert_eq!(get_prime_factors(0), vec!());
    }

    #[test]
    fn test_1() {
        assert_eq!(get_prime_factors(1), vec!());
    }

    #[test]
    fn test_2() {
        assert_eq!(get_prime_factors(2), vec!(2));
    }

    #[test]
    fn test_3() {
        assert_eq!(get_prime_factors(3), vec!(3));
    }

    #[test]
    fn test_4() {
        assert_eq!(get_prime_factors(4), vec!(2,2));
    }

    #[test]
    fn test_5() {
        assert_eq!(get_prime_factors(5), vec!(5));
    }

    #[test]
    fn test_6() {
        assert_eq!(get_prime_factors(6), vec!(2,3));
    }

    #[test]
    fn test_bignum() {
        assert_eq!(get_prime_factors(2*3*5*5*13*23), vec!(2,3,5,5,13,23));
    }
}
