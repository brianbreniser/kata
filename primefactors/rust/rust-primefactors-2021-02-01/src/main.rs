fn main() {
    println!("Hello, world!");
}

fn p(list: &mut Vec<isize>, i: isize) {
    list.push(i);
}

pub fn prime_factors(n: isize) -> Vec<isize> {
    if n <= 1 {
        return vec!();
    }

    let mut n = n;
    let mut factors: Vec<isize> = vec!();

    for i in 2..=n {
        while n % i == 0 {
            p(&mut factors, i);
            n /= i;
        }
    }

    return factors;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_0() {
        assert_eq!(prime_factors(0), vec!());
    }

    #[test]
    fn test_1() {
        assert_eq!(prime_factors(1), vec!());
    }

    #[test]
    fn test_2() {
        assert_eq!(prime_factors(2), vec!(2));
    }

    #[test]
    fn test_3() {
        assert_eq!(prime_factors(3), vec!(3));
    }

    #[test]
    fn test_4() {
        assert_eq!(prime_factors(4), vec!(2,2));
    }

    #[test]
    fn test_5() {
        assert_eq!(prime_factors(5), vec!(5));
    }

    #[test]
    fn test_6() {
        assert_eq!(prime_factors(6), vec!(2,3));
    }

    #[test]
    fn test_7() {
        assert_eq!(prime_factors(7), vec!(7));
    }

    #[test]
    fn test_8() {
        assert_eq!(prime_factors(8), vec!(2,2,2));
    }

    #[test]
    fn test_bignum() {
        assert_eq!(prime_factors(2*5*7*13*13), vec!(2,5,7,13,13));
    }
}
