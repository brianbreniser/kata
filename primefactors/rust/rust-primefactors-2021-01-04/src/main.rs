fn main() {
    println!("Hello, world!");
}

fn get_primes(x: isize) -> Vec<isize> {
    let mut primes:Vec<isize> = vec!();

    if x < 2 {
        return primes;
    }

    let mut x = x;

    for i in 2..=x {
        while x % i == 0 {
            primes.push(i);
            x = x / i;
        }
    }

    return primes;
}

#[cfg(test)]
mod test {
    use super::*;

    //primes
    // 2,3,5,7,11,13,17,19

    #[test]
    fn test0() {
        assert_eq!(get_primes(0), vec!());
    }

    #[test]
    fn test1() {
        assert_eq!(get_primes(1), vec!());
    }

    #[test]
    fn test2() {
        assert_eq!(get_primes(2), vec!(2));
    }

    #[test]
    fn test3() {
        assert_eq!(get_primes(3), vec!(3));
    }

    #[test]
    fn test4() {
        assert_eq!(get_primes(4), vec!(2,2));
    }

    #[test]
    fn test5() {
        assert_eq!(get_primes(5), vec!(5));
    }

    #[test]
    fn test6() {
        assert_eq!(get_primes(6), vec!(2,3));
    }

    #[test]
    fn test7() {
        assert_eq!(get_primes(7), vec!(7));
    }

    #[test]
    fn testbignum() {
        assert_eq!(get_primes(2*3*5*7*7*7*13*19*19), vec!(2,3,5,7,7,7,13,19,19));
    }
}
