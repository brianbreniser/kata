fn main() {
    println!("Hello, world!");
}

fn prime_factors(n: isize) -> Vec<isize> {
    if n <= 1 {
        return vec!()
    }

    let mut primes: Vec<isize> = vec!();
    let mut n = n; // shadow and make mutable internally

    for i in 2..=n {
        while n % i == 0 {
            primes.push(i);
            n /= i;
        }
    }

    return primes;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_0() {
        assert_eq!(prime_factors(0), vec!());
    }

    #[test]
    pub fn test_1() {
        assert_eq!(prime_factors(1), vec!());
    }

    #[test]
    pub fn test_2() {
        assert_eq!(prime_factors(2), vec!(2));
    }

    #[test]
    pub fn test_3() {
        assert_eq!(prime_factors(3), vec!(3));
    }

    #[test]
    pub fn test_4() {
        assert_eq!(prime_factors(4), vec!(2,2));
    }

    #[test]
    pub fn test_5() {
        assert_eq!(prime_factors(5), vec!(5));
    }

    #[test]
    pub fn test_6() {
        assert_eq!(prime_factors(6), vec!(2,3));
    }

    #[test]
    pub fn test_bignum() {
        assert_eq!(prime_factors(2*3*5*7*23), vec!(2,3,5,7,23));
    }
}
