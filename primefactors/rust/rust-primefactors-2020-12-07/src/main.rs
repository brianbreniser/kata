fn main() {
    println!("Hello, world!");
}

fn factors(n: i32) -> Vec<i32> {
    let mut n = n;
    let mut factors: Vec<i32> = vec!();

    if n <= 1 {
        return vec!();
    }

    for i in 2..=n {
        while n % i == 0 {
            factors.push(i);
            n = n / i;
        }
    }

    factors
}

#[cfg(test)]
mod tests {
    use super::*;

    // primes reminder
    // 2,3,5,7,11,13,17,19

    #[test]
    pub fn test0() {
        assert_eq!(factors(0), vec!());
    }

    #[test]
    pub fn test1() {
        assert_eq!(factors(1), vec!());
    }

    #[test]
    pub fn test2() {
        assert_eq!(factors(2), vec!(2));
    }

    #[test]
    pub fn test3() {
        assert_eq!(factors(3), vec!(3));
    }

    #[test]
    pub fn test4() {
        assert_eq!(factors(4), vec!(2,2));
    }

    #[test]
    pub fn test5() {
        assert_eq!(factors(5), vec!(5));
    }

    #[test]
    pub fn testbignum() {
        assert_eq!(factors(2*3*5*7*7*7*19), vec!(2,3,5,7,7,7,19));
    }
}
