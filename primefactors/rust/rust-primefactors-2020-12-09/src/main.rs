fn main() {
    println!("Hello, world!");
}

fn factors(n: i32) -> Vec<i32> {
    if n <= 1 {
        return vec!();
    }

    let mut n = n;
    let mut result = vec!();

    for i in 2..=n {
        while n % i == 0 {
            result.push(i);
            n = n / i;
        }
    }

    return result;
}

#[cfg(test)]
mod tests {
    use super::*;

    // primes
    // 2,3,5,7,11,13,15,17,19

    #[test]
    fn test0() {
        assert_eq!(factors(0), vec!());
    }

    #[test]
    fn test1() {
        assert_eq!(factors(1), vec!());
    }

    #[test]
    fn test2() {
        assert_eq!(factors(2), vec!(2));
    }

    #[test]
    fn test3() {
        assert_eq!(factors(3), vec!(3));
    }

    #[test]
    fn test4() {
        assert_eq!(factors(4), vec!(2,2));
    }

    #[test]
    fn test5() {
        assert_eq!(factors(5), vec!(5));
    }

    #[test]
    fn testbignum() {
        assert_eq!(factors(2*7*19*19*19), vec!(2,7,19,19,19));
    }
}
