fn main() {
    println!("Hello, world!");
}

fn factors(n: i32) -> Vec<i32> {
    if n <= 1 {
        return vec!();
    }
    let mut n = n; // shadow variable w/o change or new name

    let mut result = vec!();

    let mut i = 2;

    while i <= n {
        while n % i == 0 {
            result.push(i);
            n = n / i;
        }
        i = i + 1;
    }

    return result;
}

#[cfg(test)]
mod tests {
    use super::*;

    // primes
    // 2,3,5,7,11,13,17,19...

    #[test]
    fn factors0() {
        assert_eq!(factors(0), vec!());
    }

    #[test]
    fn factors1() {
        assert_eq!(factors(1), vec!());
    }

    #[test]
    fn factors2() {
        assert_eq!(factors(2), vec!(2));
    }

    #[test]
    fn factors3() {
        assert_eq!(factors(3), vec!(3));
    }

    #[test]
    fn factors4() {
        assert_eq!(factors(4), vec!(2,2));
    }

    #[test]
    fn factors5() {
        assert_eq!(factors(5), vec!(5));
    }

    #[test]
    fn factors6() {
        assert_eq!(factors(6), vec!(2,3));
    }

    #[test]
    fn factors7() {
        assert_eq!(factors(7), vec!(7));
    }

    #[test]
    fn factorsbighum() {
        assert_eq!(factors(2*3*5*7*11*19), vec!(2,3,5,7,11,19));
    }
}
