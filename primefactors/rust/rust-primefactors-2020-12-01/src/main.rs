fn main() {
    println!("Hello world");
}

fn get_factors(n: i64) -> Vec<i64> {
    let mut n = n.clone();
    let mut factors = vec!();
    let mut divisor = 2;

    while n > 1 {
        while n%divisor == 0 {
            factors.push(divisor);
            n = n / divisor;
        }
        divisor += 1;
    }

    return factors;
}

#[cfg(test)]
mod tests {
    use super::*;

    // reminder of lower primes
    // 2, 3, 5, 7, 11, 13, 17, 19

    #[test]
    fn factor1() {
        assert_eq!(get_factors(1), vec!());
    }

    #[test]
    fn factor2() {
        assert_eq!(get_factors(2), vec!(2));
    }

    #[test]
    fn factor3() {
        assert_eq!(get_factors(3), vec!(3));
    }

    #[test]
    fn factor4() {
        assert_eq!(get_factors(4), vec!(2, 2));
    }

    #[test]
    fn factor22357() {
        assert_eq!(get_factors(2*2*3*5*7), vec!(2,2,3,5,7))
    }
}
