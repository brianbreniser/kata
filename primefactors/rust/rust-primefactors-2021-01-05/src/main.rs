fn main() {
    println!("Hello, world!");
}

fn factors(n: isize) -> Vec<isize> {
    if n <= 1 {
        return vec!();
    }

    let mut in_n = n;
    let mut primes:Vec<isize> = vec!();

    for i in 2..=in_n {
        while in_n % i == 0 {
            primes.push(i);
            in_n = in_n / i;
        }
    }

    return primes;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test0() {
        assert_eq!(factors(0), vec!())
    }

    #[test]
    fn test1() {
        assert_eq!(factors(1), vec!())
    }

    #[test]
    fn test2() {
        assert_eq!(factors(2), vec!(2))
    }

    #[test]
    fn test3() {
        assert_eq!(factors(3), vec!(3))
    }

    #[test]
    fn test4() {
        assert_eq!(factors(4), vec!(2,2))
    }


    #[test]
    fn test5() {
        assert_eq!(factors(5), vec!(5))
    }

    #[test]
    fn testbignum() {
        assert_eq!(factors(2*2*5*13), vec!(2,2,5,13))
    }
}
