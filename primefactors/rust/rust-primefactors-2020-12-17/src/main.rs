#![feature(test)]
extern crate test;

fn main() {
    for i in 1000000..=1010000 {
        if i % 2 == 0 {
            continue;
        }
        let p = primes(i);
        println!("primes of {} is: {:?}", i, p);
    }
}

fn primes(in_n: i32) -> Vec<i32> {
    if in_n <= 1 {
        return vec!();
    }

    let mut n = in_n;
    let mut result: Vec<i32> = vec!();

    let mut i = 2;
    while i <= n {
        while n % i == 0 {
            result.push(i);
            n = n / i;
        }
        i = i + 1;
    }

    return result;
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    //primes
    // 2,3,5,7,11,13,17,19

    #[bench]
    fn test0(b: &mut Bencher) {
        b.iter(|| primes(0));
        assert_eq!(primes(0), vec!());
    }

    #[bench]
    fn test1(b: &mut Bencher) {
        b.iter(|| primes(1));
        assert_eq!(primes(1), vec!());
    }

    #[bench]
    fn test2(b: &mut Bencher) {
        b.iter(|| primes(2));
        assert_eq!(primes(2), vec!(2));
    }

    #[bench]
    fn test3(b: &mut Bencher) {
        b.iter(|| primes(3));
        assert_eq!(primes(3), vec!(3));
    }

    #[bench]
    fn test4(b: &mut Bencher) {
        b.iter(|| primes(4));
        assert_eq!(primes(4), vec!(2,2));
    }

    #[bench]
    fn test5(b: &mut Bencher) {
        b.iter(|| primes(5));
        assert_eq!(primes(5), vec!(5));
    }

    #[bench]
    fn test6(b: &mut Bencher) {
        b.iter(|| primes(6));
        assert_eq!(primes(6), vec!(2,3));
    }

    #[bench]
    fn test7(b: &mut Bencher) {
        b.iter(|| primes(7));
        assert_eq!(primes(7), vec!(7));
    }

    #[bench]
    fn test_big_num(b: &mut Bencher) {
        b.iter(|| primes(2*3*5*7*7*7*7*11*23));
        assert_eq!(primes(2*3*5*7*7*7*7*11*23), vec!(2,3,5,7,7,7,7,11,23));
    }

    #[bench]
    fn test_big_prime(b: &mut Bencher) {
        b.iter(|| primes(100999));
        assert_eq!(primes(100999), vec!(100999));
    }
}
