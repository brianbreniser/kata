fn main() {
    println!("Hello, world!");
}

fn prime_factors(n: i32) -> Vec<i32> {
    if n < 2 {
        return vec!();
    }

    // shadow n
    let mut n = n;

    let mut result: Vec<i32>= vec!();

    let mut i = 2;

    while i <= n {
        while n % i == 0 {
            result.push(i);
            n = n / i;
        }
        i = i + 1;
    }

    return result;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn factors0() {
        assert_eq!(prime_factors(0), vec!());
    }

    #[test]
    fn factors1() {
        assert_eq!(prime_factors(1), vec!());
    }

    #[test]
    fn factors2() {
        assert_eq!(prime_factors(2), vec!(2));
    }

    #[test]
    fn factors3() {
        assert_eq!(prime_factors(3), vec!(3));
    }

    #[test]
    fn factors4() {
        assert_eq!(prime_factors(4), vec!(2,2));
    }

    #[test]
    fn factors5() {
        assert_eq!(prime_factors(5), vec!(5));
    }


    #[test]
    fn factors6() {
        assert_eq!(prime_factors(6), vec!(2,3));
    }


    #[test]
    fn factorsBignum() {
        assert_eq!(prime_factors(2*3*5*7*7*11), vec!(2,3,5,7,7,11));
    }
}
