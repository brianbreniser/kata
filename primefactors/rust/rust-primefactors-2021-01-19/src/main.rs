fn main() {
    println!("Hello, world!");
}

fn primes(in_n: isize) -> Vec<isize> {
    if in_n <= 1 {
        return vec!();
    }

    let mut n = in_n;

    let mut primes: Vec<isize> = vec!();

    for i in 2..=n {
        while n % i == 0 {
            primes.push(i);
            n /= i;
        }
    }

    return primes;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_0() {
        assert_eq!(primes(0), vec!());
    }

    #[test]
    fn test_1() {
        assert_eq!(primes(1), vec!());
    }

    #[test]
    fn test_2() {
        assert_eq!(primes(2), vec!(2));
    }

    #[test]
    fn test_3() {
        assert_eq!(primes(3), vec!(3));
    }

    #[test]
    fn test_4() {
        assert_eq!(primes(4), vec!(2,2));
    }

    #[test]
    fn test_5() {
        assert_eq!(primes(5), vec!(5));
    }

    #[test]
    fn test_bignum() {
        assert_eq!(primes(2*3*3*5*7*11*11), vec!(2,3,3,5,7,11,11));
    }
}
