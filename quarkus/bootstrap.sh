#!/bin/bash

# Java sucks, bootstrapping a quarkus app is a pain. Just wrap that bullshit here

if [ $# -eq 0 ]
then
    echo "Please use with ./bootstrap.sh dirname"
    exit
fi

mvn io.quarkus:quarkus-maven-plugin:1.11.0.Final:create \
    -DprojectGroupId=org.breniser \
    -DprojectArtifactId=$1 \
    -DclassName="org.acme.getting.started.GreetingResource" \
    -Dpath="/hello"

cd $1

echo "#!/bin/bash" >> rundev.sh
echo "./mvnw compile quarkus:dev" >> rundev.sh
chmod u+x rundev.sh

echo "#!/bin/bash" >> runtests.sh
echo "./mvnw test" >> runtests.sh
chmod u+x runtests.sh

cd ..


